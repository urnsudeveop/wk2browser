#include <webkit2/webkit-web-extension.h>
#include <JavaScriptCore/JavaScript.h>
#include <iostream>
/*
static void
web_page_created_callback (WebKitWebExtension *extension,
                           WebKitWebPage      *web_page,
                           gpointer            user_data)
{
    g_print ("\nPage %d created for %s\n",
             webkit_web_page_get_id (web_page),
             webkit_web_page_get_uri (web_page));
}
*/

static void
on_uri_changed (WebKitWebPage *web_page,
                GParamSpec *pspec,
                gpointer user_data)
{
  g_print ("changed uri: %s\n", webkit_web_page_get_uri (web_page));
}

static void 
window_object_cleared_callback (WebKitScriptWorld *world, 
                                WebKitWebPage     *web_page, 
                                WebKitFrame       *frame, 
                                gpointer           user_data)
{
g_print("objCleared *");
    JSGlobalContextRef jsContext;
    JSObjectRef        globalObject;

    jsContext = webkit_frame_get_javascript_context_for_script_world (frame, world);
    globalObject = JSContextGetGlobalObject (jsContext);

printf("\njsContext %u\n",jsContext);
 // JSCValue* title = jsc_context_evaluate(jsContext, "document.title;", -1);

//  puts(jsc_value_to_string(title));
 //while(1){};
g_print("objCleared **");

}
static void
web_page_created_callback (WebKitWebExtension *extension,
                           WebKitWebPage      *web_page,
                           gpointer            user_data)
{
    g_signal_connect (web_page,
                      "notify::uri",
                      G_CALLBACK (on_uri_changed),
                      NULL);
    g_print ("created new web_view widget with id %d created\n",
             webkit_web_page_get_id (web_page));
			
}

extern "C" G_MODULE_EXPORT void
webkit_web_extension_initialize (WebKitWebExtension *extension)
{
	g_print("webkit_web_extension_initialize\n");
	
	
	
    g_signal_connect (extension, "page-created",
                      G_CALLBACK (web_page_created_callback),
                      NULL);
					  
					    g_signal_connect (webkit_script_world_get_default (), 
                      "window-object-cleared", 
                      G_CALLBACK (window_object_cleared_callback), 
                      NULL);
}

#include <gtkmm.h>
//#include <webkit2/webkit-web-extension.h>
//#include <./test.cpp>
#include <webkit2/webkit2.h>
#include <JavaScriptCore/JavaScript.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>
#include <jsc/jsc.h>
#include "inc/events.c"
using namespace std;

class Sample {
public:
    Sample(int i) { this->i = i; };
    JSCValue* toJSObject(JSCContext* jsContext);
    int i;
};

JSCValue* Sample::toJSObject(JSCContext* jsContext)
{
    JSCValue* ret = jsc_value_new_object(jsContext, nullptr, nullptr); 

    JSCValue* i = jsc_value_new_number(jsContext, this->i);
    jsc_value_object_define_property_data(ret, "i", JSC_VALUE_PROPERTY_ENUMERABLE, i);

    return ret;
}

class UrnusdevBrowser
{
public:
WebKitWebContext* context;
WebKitWebView * one ;	
GtkWidget *wv;
//WebKitWebFrame *frame
//WebKitFrame* frame;
WebKitWebResource * wkr;
WebKitUserContentManager *UserContentManager;
WebKitSettings * setting;
JSCContext* jsContext;
Glib::RefPtr<Gtk::Application> app;
char *uri;
char *script;

int width;
int height;
char cmd [512000];
char buffer_script[512000];
char scr_script[512000];
std::string script_src;
int test;
//WebKitWebPage wkp;

};

pthread_t idHilo;
pthread_t idHilo2;
size_t esize;

UrnusdevBrowser UB;



static void
web_view_javascript_finished (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{

puts("--- into web_view_javascript_finished");
    WebKitJavascriptResult *js_result;
    JSCValue               *value;
    GError                 *error = NULL;

    js_result = webkit_web_view_run_javascript_finish (WEBKIT_WEB_VIEW (object), result, &error);
    if (!js_result) {
        g_warning ("Error running javascript: %s", error->message);
        g_error_free (error);
        return;
    }
 //UB.jsContext = (JSCContext*) webkit_javascript_result_get_global_context (js_result);
 
    value = webkit_javascript_result_get_js_value (js_result);
UB.jsContext = jsc_value_get_context (value);


g_autoptr(JSCValue) ev=jsc_context_evaluate(UB.jsContext, "var ude=1;",10);

//printf("context jsc_value_get_context (value) %u\n",UB.jsContext);
    if (jsc_value_is_string (value)) {
        JSCException *exception;
        gchar        *str_value;

        str_value = jsc_value_to_string (value);

printf("context jsc_value_get_context (value) %u\n",jsc_value_get_context (value));


        exception = jsc_context_get_exception (jsc_value_get_context (value));
        if (exception)
            g_warning ("Error running javascript: %s", jsc_exception_get_message (exception));
        else
            g_print ("Script result: %s\n", str_value);
        g_free (str_value);
    } else {
        g_warning ("Error running javascript: unexpected return value");
    }
    webkit_javascript_result_unref (js_result);
}

int on_cmd(const Glib::RefPtr<Gio::ApplicationCommandLine> &,
  Glib::RefPtr<Gtk::Application> &app) {

app->activate();

return 0;
   
}


void initialize_web_extensions(WebKitWebContext* context, gpointer user_data) {
    g_print("\n[webkit2 web-extensions] initialize-web-extensions\n");
    webkit_web_context_set_web_extensions_directory(context, ".");
//	webkit_web_context_
// set_web_extensions_initialization_user_data(GLib.Variant.new_string("test string"));
    return;
}
void load_signals(){


g_signal_connect(G_OBJECT(UB.one),"focus-in-event",G_CALLBACK(view_focus_in_event),NULL);
g_signal_connect(G_OBJECT(UB.one),"focus-out-event",G_CALLBACK(view_focus_out_event),NULL);

//g_signal_connect(G_OBJECT(UB.one),"dnd-finished",G_CALLBACK(view_drag_drop),NULL);
g_signal_connect(G_OBJECT(UB.one),"drag-data-received",G_CALLBACK(view_drag_data_received),NULL);


g_signal_connect(G_OBJECT(UB.one),"key-press-event",G_CALLBACK(view_keypress),NULL);
g_signal_connect(G_OBJECT(UB.one),"load-changed",G_CALLBACK(web_view_load_changed),NULL);

//g_signal_connect(G_OBJECT(UB.wv),"clicked",G_CALLBACK(view_clicked),NULL);


//g_signal_connect(G_OBJECT(UB.wv),"window-object-cleared",G_CALLBACK(window_object_cleared),NULL);


}

void *brow(void *arg)
{

int arggtk;
char **argvgtk;
 GtkWidget *windowx;
UB.app = Gtk::Application::create( arggtk, argvgtk, "",Gio::APPLICATION_HANDLES_COMMAND_LINE );
  Gtk::Window window;  

  window.set_default_size( UB.width, UB.height );
puts("antes\n");
UB.context = webkit_web_context_get_default();
puts("despues\n");
g_signal_connect(UB.context, "initialize-web-extensions", G_CALLBACK(initialize_web_extensions), NULL);

UB.wv=webkit_web_view_new_with_context(UB.context);
//UB.wv=   webkit_web_view_new() ;
  UB.one = WEBKIT_WEB_VIEW( UB.wv);


load_signals();


UB.setting = webkit_web_view_get_settings(UB.one);
    webkit_settings_set_enable_page_cache(UB.setting, true);
    webkit_settings_set_enable_javascript(UB.setting, true);
  webkit_settings_set_enable_tabs_to_links(UB.setting, true);
    webkit_settings_set_enable_offline_web_application_cache(UB.setting, false);
    webkit_settings_set_allow_universal_access_from_file_urls(UB.setting,true);    
g_object_set
(UB.setting,
		"javascript_can_open_windows_automatically",TRUE,
		"allow-universal-access-from-file-urls" ,TRUE,
		"enable-plugins", TRUE,
		"print-backgrounds", TRUE,
		"enable-javascript", TRUE,
		"enable-javascript-markup",TRUE,
		"enable-developer-extras",TRUE,
		"enable-html5-database", TRUE,
		"enable-html5-local-storage", TRUE,
		"enable-caret-browsing",TRUE,
		"enable-java", TRUE,
		"enable-page-cache", FALSE,
		"enable-write-console-messages-to-stdout", TRUE,
		"enable-offline-web-application-cache", FALSE,
		"zoom-text-only", FALSE,
		"media-playback-requires-user-gesture", TRUE, 
		"user-agent", "BCODE COMPOSER",
        NULL
	);



UB.UserContentManager = webkit_web_view_get_user_content_manager(UB.one);
printf("\nOpening url:%s",UB.uri);

  Gtk::Widget * three = Glib::wrap( GTK_WIDGET( UB.one ) );

  window.add( *three );
  webkit_web_view_load_uri(UB.one, UB.uri);

  window.show_all();
  webkit_web_view_run_javascript(UB.one,UB.script,NULL,web_view_javascript_finished,NULL);

    UB.app->signal_command_line().connect(
sigc::bind(sigc::ptr_fun(on_cmd), UB.app), false
);

  UB.app->run( window );  

exit( 0 ); 

}


int clear_icanon(void)
{
  struct termios settings;
  int result;
  result = tcgetattr (STDIN_FILENO, &settings);
  if (result < 0)
    {
      perror ("error in tcgetattr");
      return 0;
    }

  settings.c_lflag &= ~ICANON;

  result = tcsetattr (STDIN_FILENO, TCSANOW, &settings);
  if (result < 0)
    {
      perror ("error in tcsetattr");
      return 0;
   }
  return 1;
}




int main( int argc
        , char *argv[]
        )
{
clear_icanon();

char uri[]="uri";
char script[]="script";
char endscript[]="endscript";
char reload[]="reload";
gchar *gscript;
gchar script_buffer[512000];


UB.uri=argv[1];
UB.script=argv[2];
if(argv[3]!=NULL)
{
UB.width=atoi(argv[3]);
}else
{
UB.width=800;
}

if(argv[4]!=NULL){
UB.height=atoi(argv[4]);
}else
{
UB.height=600;
}


pthread_create (&idHilo, 0, brow, NULL); 

//gtk_widget_grab_focus(GTK_WIDGET(UB.one));
/*scanf("%[^\n]", UB.cmd) ;
printf("------------>%s\n",UB.cmd);*/
while(1){
puts("\ncmd:\n");

cin  >> UB.cmd;


 // g_signal_emit_by_name(UB.wv, "window-object-cleared", NULL);
//UB.jsContext=jsc_context_new();

//UB.jsContext = jsc_context_get_current ();//jsc_context_new();
/*if (JSC_IS_CONTEXT(UB.jsContext))
{
puts("\nescontexto\n");
}*/
//printf("context %u\n",UB.jsContext);
/*
char alert[]="alert('q')";
 g_autoptr(JSCValue) jsVar = jsc_context_evaluate(UB.jsContext, alert,-1);//strlen(alert));
//Sample obj(42);
g_autofree char *result_string = jsc_value_to_string(jsVar);
//JSCValue* ret = jsc_value_function_call(jsVar, G_TYPE_NONE);
     g_print("Result: %s\n", result_string);
*/
//UB.jsContext=webkit_frame_get_js_context (UB.one);


/*jsc_context_evaluate_with_source_uri (UB.jsContext,
                                     "alert('asd');",
					13,
                                      "file:///home/urnusdev/PascalFormat.htm",
                                      1);

*/
/*JSCValue* jsDiv = jsc_context_evaluate(jsContext, "$('some-other-div')",19);
jsc_value_object_invoke_method(jsVar, "html", G_TYPE_OBJECT, jsDiv, G_TYPE_NONE);
JSCException* exception = jsc_context_get_exception(jsContext);
if (exception != NULL) {
  g_print("%s: %s\n", jsc_exception_get_name(exception),
                      jsc_exception_get_message(exception));
  free(exception);
//  return;
}*/

WebKitWebViewPrivate* privateData = UB.one->priv;
//printf(" tama cin %i\n",strlen(UB.cmd));
/*Load webpage*/

if (strcmp (UB.cmd,uri) == 0)
{
puts("Ingrese url incluido el protocolo (http:// o https://):");
//g_signal_connect(UB.context, "initialize-web-extensions", G_CALLBACK(initialize_web_extensions), NULL);
cin >> UB.cmd;
//printf("\nOpening url:%s\n",UB.cmd);
 webkit_web_view_load_uri(UB.one, UB.cmd); 
//gtk_widget_grab_focus(GTK_WIDGET(UB.one));
}



/*inject javascript*/

if (strcmp (UB.cmd,script) == 0)
{
	puts("finalice con endscript\n");

		while( strcmp (endscript,  (char *)UB.script_src.c_str()) != 0 )


//esize=512000;		
	{
getline(cin, UB.script_src);
//scanf("%[^\n]", gscript) ;
//int bytes_leidos = getline(&UB.scr_script, &esize, cin);
 //cin.getline (gscript, 512000);
int tama=strlen((char *)UB.script_src.c_str());
//(char *)UB.script_src.c_str()
				if(strcmp (endscript, (char *)UB.script_src.c_str()) != 0)

	{

gscript=(gchar *)UB.script_src.c_str();//"alert('uno dos tres'); alert('cuatro cinco seis');";

int sizescript=strlen(gscript);
printf("size:%i\n",sizescript);
gscript[sizescript+1]=0x10;
int sizesb=strlen(script_buffer);

for(int i=0;i<sizescript;i++)
{
try
{


script_buffer[sizesb+i]=gscript[i];

}catch(int e)
{
printf("error controlado %1",e);
}

}

		
	}

}
script_buffer[strlen(script_buffer)+1]=0x00;
/**/

GError                 *error = NULL;

/*
g_signal_emit_by_name(G_OBJECT(UB.one), "activate", NULL);
 g_signal_emit_by_name(G_OBJECT(UB.one), "clicked", NULL);
*/
/*
g_signal_emit_by_name(G_OBJECT(UB.one), "load-changed",WEBKIT_LOAD_FINISHED);
g_signal_emit_by_name(UB.one, "ready-to-show",0);*/


/*
webkit_web_view_run_javascript (UB.UserContentManager,
                                script_buffer,NULL,NULL,&error);
*/

//UB.UserContentManager = webkit_web_view_get_user_content_manager (UB.one);

				WebKitUserScript *script = webkit_user_script_new
								(
		 						script_buffer, 
//WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES
								WEBKIT_USER_CONTENT_INJECT_TOP_FRAME, 
//WEBKIT_USER_SCRIPT_INJECT_AT_DOCUMENT_START,
								WEBKIT_USER_SCRIPT_INJECT_AT_DOCUMENT_END, 
								NULL, NULL
								);
                                
	webkit_user_content_manager_add_script
	(
		UB.UserContentManager, 
		script
	);
        
        
	webkit_user_script_unref(script);
WebKitJavascriptResult *js_result;
char wname[]="npi";

puts("recargue con reload para ejecutar el script insertado");

memset(script_buffer, 0, sizeof script_buffer);
UB.script_src="";

}

if (strcmp (UB.cmd,reload) == 0)
{
webkit_web_view_reload (UB.one);

}

};
}


/*
compilar:
g++ browser.cpp `pkg-config gtkmm-3.0 --libs --cflags` `pkg-config webkit2gtk-4.0 --libs --cflags` -o browser ; chmod +x browser
*/

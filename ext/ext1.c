#include <webkit2/webkit-web-extension.h>
#include <JavaScriptCore/JavaScript.h>
#include <iostream>

static void window_object_cleared_callback(WebKitScriptWorld *world, WebKitWebPage *webPage, WebKitFrame *frame, gpointer userData)
{
  std::cout << "Callback reached\n";

  JSCContext *jsCtx = webkit_frame_get_javascript_context_for_script_world(frame);
  JSCValue* title = jsc_context_evaluate(jsCtx, "document.title;", -1);

  std::cout << "Title: " << jsc_value_to_string(title) << "\n";
}

extern "C" G_MODULE_EXPORT void webkit_web_extension_initialize(WebKitWebExtension* extension)
{
  g_signal_connect(webkit_script_world_get_default(), "window-object-cleared", G_CALLBACK(window_object_cleared_callback), NULL);
}
